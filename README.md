This is an assignment from my operating systems class. The task was to write the getword function().

The getword() function reads a word from the input stream and outputs the number of characters in the word.
It also takes a pointer to the beginning of a char array and assembles a "word" as it reads each characters.
Exceptions are whitespaces, which are ignored, newline, which returns 0, EOF, which returns -1, and the string "done" which returns -1.
Metacharacters "<", ">", ">&", ">>", ">>&", "|", "#", and "&" are treated as delimiters and are output as is (collection of these metachars is greedy).
If a backslash character preceds a metacharacter or a space, they will be treated as a regular character. Otherwise backslashes are ignored.
Finally, if a  word scanned is longer than 254 characters, the function outputs a string consisting consisting of these characters. The remaining parts
of the word is used in a new  getword() call.